## `provideCabal' and `provideGHC' are on by default, but
## you should disable them if you have your own installs/want something
## more up to date.

## export CPATH=~/.nix-profile/include; export LIBRARY_PATH=~/.nix-profile/lib
## I have no idea why zlib isn't already setting the include headers properly.

{ pkgs ? import <nixpkgs> {}, provideCabal ? true, provideGHC ? true }:

let haskellTools = with pkgs;
  (if provideCabal then [ cabal-install ] else []) ++
  (if provideGHC then [ haskell.compiler.ghc865 ] else []);

in pkgs.mkShell {
  buildInputs = with pkgs; [
    pkg-config
    zlib.dev
    zlib.out
    cairo
  ] ++ haskellTools;
}
