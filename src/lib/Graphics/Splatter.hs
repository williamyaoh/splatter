{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Graphics.Splatter
  ( module Graphics.Splatter.Internal
  , module Graphics.Splatter.Toplevel
  )
where

import Graphics.Splatter.Internal
import Graphics.Splatter.Toplevel
