{-# LANGUAGE RecordWildCards #-}

module Graphics.Rendering.Cairo.Extended
  (
    -- * Colors.
    Color(..)
  , color255
  )
where

import Data.Word ( Word8 )

import Graphics.Rendering.Cairo

-- |
-- Store color information as RGB information, from 0-255 for each dimension.
-- This makes it easier to e.g. use CSS hexadecimal-style colors with Cairo,
-- as we can directly write the hex literals instead of needing to scale between
-- 0.0-1.0.
data Color = Color
  { colorR :: {-# UNPACK #-} !Word8
  , colorG :: {-# UNPACK #-} !Word8
  , colorB :: {-# UNPACK #-} !Word8
  }
  deriving (Eq, Show)

-- |
-- Set the current fill color. Slightly low level, as it takes in the RGB
-- values directly, as values in the range 0-255. Prefer to use @colorRGB@ if
-- possible.
color255 :: Word8 -> Word8 -> Word8 -> Render ()
color255 r g b =
  setSourceRGB (fi r / 255) (fi g / 255) (fi b / 255)
  where fi = fromIntegral
