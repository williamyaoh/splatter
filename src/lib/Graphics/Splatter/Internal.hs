{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures             #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}

-- |
-- This module is something of a hodgepodge of different tools needed to
-- ease creating new generators.
--
-- Some things you might find here:
--
--   * Common data types, like coordinates
--   * Tools for capturing command-line parameters
--   * Ways to ease randomly generating input parameters

module Graphics.Splatter.Internal
  ( Coord(..)
  , Color(..)
  , colorRGB
  , circle
  , GenArt'
  , runGenArt
  , exploreM
    -- * Configuration and ingesting CLI parameters.
  , MetaB(..), Meta
  , getSeed, getDimensions, readerConfig
  , Sourced(..)
  , combineConfig, unsourceConfig
  , (<||>), unsource
    -- * Reexports for working with our types.
  , module Linear.V2
  , module Barbies
  , module Barbies.Bare
  , module GHC.Generics
  , Identity(..)
  , Rand, StdGen, getRandom, getRandomR
  )
where

import Barbies      hiding ( Unit, Void )
import Barbies.Bare

import           Data.Functor.Identity
import qualified Data.Set              as Set

import Control.Monad.Random
import Control.Monad.Trans.Reader

import Linear.V2

import GHC.Generics hiding ( Meta )

import Graphics.Rendering.Cairo
import Graphics.Rendering.Cairo.Extended

--------------------
-- DATA TYPES
--------------------

-- |
-- Splatter-specific 2D points.
--
-- While we want our calculations to have as much precision as possible, we
-- need to be able to compare only on the first decimal digit of precision
-- to be able to deduplicate effectively.
newtype Coord = Coord (V2 Double)
  deriving newtype (Show, Num)

-- Thankfully we only care about pixel scale, so we can just round everything
-- to nearest integer before comparing.

instance Eq Coord where
  (==) (Coord v1) (Coord v2) = normalize v1 == normalize v2
    where normalize :: V2 Double -> V2 Int
          normalize = fmap round

instance Ord Coord where
  compare (Coord v1) (Coord v2) = normalize v1 `compare` normalize v2
    where normalize :: V2 Double -> V2 Int
          normalize = fmap round

-- |
-- Our monad stack.
type GenArt' cfg a = ReaderT (Meta cfg) (Rand StdGen) a

-- |
-- Do some random generation. This is specifically structured this way in order
-- to separate the generation of the output data from the rendering of said
-- output data, to make our generators easier to test.
runGenArt :: Meta cfg -> StdGen -> GenArt' cfg a -> a
runGenArt cfg gen =
  flip evalRand gen . flip runReaderT cfg

--------------------
-- RENDERING
--------------------

-- |
-- Draw a path with the given center and radius. Doesn't create a new path.
circle :: Coord -> Double -> Render ()
circle (Coord (V2 x y)) radius = arc x y radius 0 (2 * pi)

-- |
-- Set the current fill color.
colorRGB :: Color -> Render ()
colorRGB (Color { .. }) = color255 colorR colorG colorB

--------------------
-- SOURCING PARAMETERS
--------------------

-- |
-- While each art generator will have its own specific parameters, some are
-- common to all of them, such as RNG seed and image dimensions.
data MetaB cfg t f = Meta
  { metaCfg        :: cfg
  , metaSeed       :: Wear t f Int
  , metaDimensions :: Wear t f (Int, Int)
  }
  deriving (Generic)

type Meta cfg = MetaB cfg Bare Identity

deriving instance Show cfg => Show (MetaB cfg Bare a)
deriving instance FunctorB (MetaB cfg Covered)
deriving instance BareB (MetaB cfg)

getSeed :: GenArt' cfg Int
getSeed = reader metaSeed

getDimensions :: GenArt' cfg (Int, Int)
getDimensions = reader metaDimensions

readerConfig :: (cfg -> a) -> GenArt' cfg a
readerConfig f = reader (f . metaCfg)

-- |
-- Tells where the parameter for our random generation came from.
-- User-specified params should take priority, followed by random params,
-- then defaults.
data Sourced a
  = Specified a
  | Generated (Rand StdGen a)
  | Default a
  deriving (Functor)

-- |
-- Combine two sourced parameters, giving precendence to user-specified
-- parameters, and then randomly-generated ones.
(<||>) :: Sourced a -> Sourced a -> Sourced a
(<||>) s@(Specified _) _ = s
(<||>) _ s@(Specified _) = s
(<||>) g@(Generated _) _ = g
(<||>) _ g@(Generated _) = g
(<||>) l _               = l

unsource :: Sourced a -> Rand StdGen a
unsource (Default a)   = pure a
unsource (Specified a) = pure a
unsource (Generated g) = g

-- |
-- Combine the configurations, keeping in mind where the parameters came from.
combineConfig :: ApplicativeB (cfg Covered)
              => cfg Covered Sourced
              -> cfg Covered Sourced
              -> cfg Covered Sourced
combineConfig = bzipWith (<||>)

-- |
-- Produce a 'final' config from our sources, which might require some
-- random generation to get all the parameters.
unsourceConfig :: (TraversableB (cfg Covered), BareB cfg)
               => cfg Covered Sourced
               -> Rand StdGen (cfg Bare Identity)
unsourceConfig = fmap bstrip . btraverse (fmap Identity . unsource)

--------------------
-- FINDING NEIGHBORING OBJECTS
--------------------

-- |
-- "Explore" a graph and find the transitive closure based on the
-- transition function given, starting from the given node. Note
-- that the transitive closure must be finite, or this won't terminate.
exploreM :: forall m a. (Monad m, Ord a) => (a -> m [a]) -> a -> m [a]
exploreM transitions start =
  Set.toAscList <$> transClosureM (Set.singleton start)
  where transClosureM :: Set.Set a -> m (Set.Set a)
        transClosureM prev = do
          outs <- Set.union prev . Set.fromList . mconcat <$> mapM transitions (Set.toAscList prev)
          if outs == prev
            then pure outs
            else transClosureM outs
