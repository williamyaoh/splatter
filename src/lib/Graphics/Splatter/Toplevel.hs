{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTSyntax                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE CPP                       #-}

module Graphics.Splatter.Toplevel where

import Data.Maybe            ( fromMaybe )
import Data.Time.Clock.POSIX as Time
import Data.Void
import Data.Word             ( Word8 )

import Control.Monad.Random

import Graphics.Rendering.Cairo
import Graphics.Splatter.Internal

import Text.Megaparsec      as Mega hiding ( option )
import Text.Megaparsec.Char as Mega

import Options.Applicative as Opt hiding ( many, some )

-- Removing imports for GHC 8.8.X+
#if MIN_VERSION_base(4,13,0)
#else
import Control.Monad ( void )
#endif

data Generator where
  Generator :: forall cfg output. (BareB cfg, ApplicativeB (cfg Covered), TraversableB (cfg Covered))
            => Opt.Parser (cfg Covered Sourced)            -- ^ How to parse config from command line
            -> cfg Covered Sourced                         -- ^ Defaults, including random generation
            -> GenArt' (cfg Bare Identity) output          -- ^ How to generate objects
            -> (Meta (cfg Bare Identity) -> output -> Render ())    -- ^ How to render output to a surface
            -> Generator

execGenerator :: [String] -> Generator -> IO ()
execGenerator cliArgs (Generator cli def gen render) = do
  seed <- round . (* 1000) <$> Time.getPOSIXTime
  let parser = Opt.info (metaizeCLI cli <**> helper) mempty
  let parseResult = Opt.execParserPure defaultPrefs parser cliArgs
  fromCLI <- Opt.handleParseResult parseResult
  let defaulted = Meta
        { metaCfg = def
        , metaSeed = Identity seed
        , metaDimensions = Identity (700, 200)
        }
  let finalMeta = combineMeta defaulted fromCLI

  let stdgen = mkStdGen (metaSeed finalMeta)

  let (innerCfg, stdgen') = runRand (unsourceConfig (metaCfg finalMeta)) stdgen

  let input = finalMeta { metaCfg = innerCfg }

  let output = runGenArt input stdgen' gen

  let render' = render input output

  let (w, h) = metaDimensions input

  withImageSurface FormatARGB32 w h $ \surf -> do
    renderWith surf $ render'
    surfaceWriteToPNG surf "splatter.png"
  withSVGSurface "splatter.svg" (fromIntegral w) (fromIntegral h) $ \surf ->
    renderWith surf $ render'

combineMeta :: ApplicativeB (cfg Covered)
            => MetaB (cfg Covered Sourced) Covered Identity
            -> MetaB (cfg Covered Sourced) Covered Maybe
            -> MetaB (cfg Covered Sourced) Bare Identity
combineMeta def mMeta =
  -- The easiest thing to do is to just combine these manually.
  Meta
    { metaCfg = metaCfg def `combineConfig` metaCfg mMeta
    , metaSeed = runIdentity (metaSeed def) `fromMaybe` metaSeed mMeta
    , metaDimensions = runIdentity (metaDimensions def) `fromMaybe` metaDimensions mMeta
    }

metaizeCLI :: Opt.Parser a -> Opt.Parser (MetaB a Covered Maybe)
metaizeCLI parser = Meta
  <$> parser
  <*> optional auto (long "seed" <> short 's')
  <*> optional (maybeReader $ Mega.parseMaybe dimensions)
        (long "dimensions" <> short 'd')
  where optional :: ReadM a -> Mod OptionFields (Maybe a) -> Parser (Maybe a)
        optional read mod = option (fmap Just read) (mod <> value Nothing)

--------------------
-- PARSERS
--------------------

-- | Low-level parser that provides a sequence of digits.
digits :: Mega.Parsec Void String [Word8]
digits = some (read . pure <$> Mega.digitChar)

positive :: Mega.Parsec Void String Int
positive = digitize 0 <$> digits
  where digitize :: Int -> [Word8] -> Int
        digitize n []     = n
        digitize n (x:xs) = digitize (n * 10 + fromIntegral x) xs

dimensions :: Mega.Parsec Void String (Int, Int)
dimensions = do
  w <- positive
  void $ many spaceChar *> char ',' *> many spaceChar
  h <- positive
  pure (w, h)
