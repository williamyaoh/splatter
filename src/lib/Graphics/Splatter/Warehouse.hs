{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveFunctor      #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE KindSignatures     #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications   #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE CPP                #-}

-- |
-- Generates images containing 3D cubes.

module Graphics.Splatter.Warehouse
  ( generator
  , ConfigB(..), Config
    -- * Monad stack.
  , GenArt
  , getRadius, getCTCFactor, getCTC
  , getSpineLength, getNorth, getShrinkFactor
    -- * Data types.
  , FillStyle(..), Dot, Panel(..), Box(..)
    -- * Sourcing configuration parameters.
  , defaultConfig, cliOptions
  )
where

import           Data.Bifunctor
import           Data.Functor.Identity
import           Data.Vector           ( Vector )
import qualified Data.Vector           as Vec

import Control.Monad.Extra  ( unfoldM )
import Control.Monad.Random

import qualified Numeric.Noise.Perlin as Perlin

import Linear.Matrix          ( (!*) )
import Linear.Matrix.Extended ( rotation2D )

import Options.Applicative as Opt

import Graphics.Rendering.Cairo
import Graphics.Splatter.Internal
import Graphics.Splatter.Toplevel

-- Removing imports for GHC 8.8.X+
#if MIN_VERSION_base(4,13,0)
#else
import Control.Monad        ( (>=>) )
#endif

--------------------
-- WAREHOUSE CONFIGURATION
--------------------

-- |
-- Warehouse-specific configuration.
data ConfigB t f = Config
  { cfgRadius       :: Wear t f Double      -- ^ how large should the boxes be?
  , cfgCTCFactor    :: Wear t f Double      -- ^ factor >=1 for how much extra space between boxes
  , cfgSpineLength  :: Wear t f Int         -- ^ how many circles should each panel extend from the center?
  , cfgNorth        :: Wear t f (V2 Double) -- ^ unit vector for box orientation
  , cfgShrinkFactor :: Wear t f Double      -- ^ how much to decrease the size of dots from directly
  }
  deriving (Generic)

type Config = ConfigB Bare Identity

-- TODO 2020-01-30: These derives are seriously slowing down compilation.
--   Consider using -ddump-deriv and adding in the instances manually.

deriving instance Show (ConfigB Bare a)
deriving instance FunctorB (ConfigB Covered)
deriving instance ApplicativeB (ConfigB Covered)
deriving instance TraversableB (ConfigB Covered)
deriving instance BareB ConfigB

--------------------
-- MONAD STACK
--------------------

type GenArt a = GenArt' Config a

getRadius :: GenArt Double
getRadius = readerConfig cfgRadius

getCTCFactor :: GenArt Double
getCTCFactor = readerConfig cfgCTCFactor

getCTC :: GenArt Double
getCTC = liftA2 (\r f -> ctc r * f) getRadius getCTCFactor

getSpineLength :: GenArt Int
getSpineLength = readerConfig cfgSpineLength

getNorth :: GenArt (V2 Double)
getNorth = readerConfig cfgNorth

getShrinkFactor :: GenArt Double
getShrinkFactor = readerConfig cfgShrinkFactor

--------------------
-- WAREHOUSE DATA
--------------------

data FillStyle = Fill | Hollow
  deriving (Eq, Show)

type Dot = (Coord, Double, FillStyle, Maybe Color)  -- ^ center, radius, fill style, color override

data Panel = Panel
  { panelDots  :: !(Vector Dot)
  , panelColor :: !Color
  }
  deriving (Eq, Show)

data Box = Box
  { boxCenter :: !Coord
  , boxTop    :: !Panel
  , boxLeft   :: !Panel
  , boxRight  :: !Panel
  }
  deriving (Eq, Show)

--------------------
-- TOPLEVEL GENERATOR
--------------------

generator :: Generator
generator = Generator
  cliOptions
  defaultConfig
  genOutput
  render

--------------------
-- SOURCING CONFIGURATION PARAMETERS
--------------------

defaultConfig :: ConfigB Covered Sourced
defaultConfig = Config
  { cfgRadius       = Generated $ getRandomR (70, 110)
  , cfgCTCFactor    = Generated $ getRandomR (1.0, 1.2)
  , cfgSpineLength  = Default 6
  , cfgNorth        = Generated $ angle <$> getRandomR (0, 2 * pi)
  , cfgShrinkFactor = Default 0.9
  }

cliOptions :: Opt.Parser (ConfigB Covered Sourced)
cliOptions = Config
  <$> option (Specified <$> auto) (long "radius" <> short 'r' <> value (Default 100))
  <*> option (Specified <$> auto) (long "ctc-factor" <> value (Default 1.1))
  <*> option (Specified <$> auto) (long "spine-length" <> value (Default 6))
  <*> option (Specified . angle @Double <$> auto)
        (long "north" <> value (Default $ angle 0) <>
          help "axis of orientation for box generation. specify as angle from 0 to 2π")
  <*> option (Specified <$> auto) (long "shrink-factor" <> value (Default 0.9) <>
               help "how much to shrink the dots to avoid overlap. specify as factor 0.0-1.0")

--------------------
-- 2D CALCULATIONS
--------------------

-- |
-- Given a specific box radius, what is the minimum center-to-center distance
-- between two boxes?
ctc :: Double -> Double
ctc radius = cos (pi / 6) * radius * 2

outsideBoundingBox :: (Double, Double) -> Double -> Coord -> Bool
outsideBoundingBox (w, h) radius (Coord (V2 x y)) =
  -- We don't need to be amazingly precise with this calculation.
  -- Some out-of-bounds drawings are fine.
  let xout = ((0 - x) `max` 0) `max` ((x - w) `max` 0)
      yout = ((0 - y) `max` 0) `max` ((y - h) `max` 0)
  in xout > 2 * radius || yout > 2 * radius

coordsAround :: V2 Double -> Double -> Coord -> [Coord]
coordsAround north centerToCenter (Coord center) = do
  let rotations = [ rotation2D (pi * (i / 3)) | i <- [0..5] ]
  fmap (\rot -> Coord $ center + (rot !* north) * pure centerToCenter) rotations

--------------------
-- RANDOM GENERATION
--------------------

-- |
-- Our 'top-level' program, which is responsible for actually producing
-- the boxes to render, given some initial state. Whoever calls this is
-- responsible for setting parameters appropriately, including randomly
-- modifying the starting parameters if desired by the user.
genOutput :: GenArt [Box]
genOutput = do
  (w, h) <- bimap fi fi <$> getDimensions
  (x, y) <- liftA2 (,) (getRandomR (-10, 10)) (getRandomR (-5, 5))
    -- A random adjustment to the starting position.

  let startCoord = Coord (V2 (w // 2 + x) (h // 2 + y))

  allCoords <- exploreM validCoords startCoord
  allBoxes <- mapM genBox allCoords

  pure allBoxes
  where fi :: Int -> Double
        fi = fromIntegral

        (//) :: Double -> Double -> Double
        (//) n divisor = fi (round (n / divisor))

-- |
-- Generate the six coordinates around a given coordinate, taking into account
-- box radius and current north orientation for our generation session.
validCoords :: Coord -> GenArt [Coord]
validCoords coord = do
  (w, h) <- bimap fi fi <$> getDimensions
  north <- getNorth
  radius <- getRadius
  ctc <- getCTC

  pure $
    Prelude.filter (not . outsideBoundingBox (w, h) radius) $
      coordsAround north ctc coord
  where fi :: Int -> Double
        fi = fromIntegral

-- |
-- In order to not make things jut up against each other, we want to shrink
-- the dots by default.
dotShrink :: Dot -> GenArt Dot
dotShrink (center, radius, fill, color) = do
  factor <- getShrinkFactor
  pure (center, radius * factor, fill, color)

-- |
-- Randomly adjust the size of our dots to make them look more interesting.
dotDistort :: Dot -> GenArt Dot
dotDistort (Coord (V2 x y), radius, fill, color) = do
  seed <- fromIntegral <$> getSeed

  let perlinOctaves = 5
      perlinScale = 0.1
      perlinPersistance = 0.5
      perlinNoise = Perlin.perlin (round seed) perlinOctaves perlinScale perlinPersistance
  let adjustment = Perlin.noiseValue perlinNoise (x + seed, y + seed, seed)

  pure (Coord (V2 x y), radius + adjustment, fill, color)

-- |
-- Make some dots hollow, others solid.
dotRandomizeFill :: Dot -> GenArt Dot
dotRandomizeFill (center, radius, _, color) = do
  fill <- fromList [ (Fill, 0.8), (Hollow, 0.2) ]
  pure (center, radius, fill, color)

-- |
-- Occasionally make some dots in a panel accent colors.
dotRandomizeColor :: Dot -> GenArt Dot
dotRandomizeColor (center, radius, fill, _) = do
  color <- fromList [ (Just gunmetal, 0.05)
                    , (Nothing, 0.95)
                    ]
  pure (center, radius, fill, color)

genPanelDots :: Coord -> V2 Double -> GenArt (Vector Dot)
genPanelDots (Coord center) unit = do
  boxRadius <- getRadius
  spineLength <- fromIntegral <$> getSpineLength
  let dotRadius = boxRadius / spineLength / 2
      leftVec = (rotation2D (2 * pi / 3) !* unit) * pure (2 * dotRadius)
      rightVec = (rotation2D (negate $ 2 * pi / 3) !* unit) * pure (2 * dotRadius)
      buildSpine offset = do
        let dotCenter = center + (pure (dotRadius + (dotRadius * 2 * offset)) * unit)
        pure (Coord dotCenter, dotRadius, Fill, Nothing)
  spine <- Vec.fromList <$> mapM buildSpine [0..spineLength-1]
  extendedLeft <- unfoldM (extend leftVec) spine
  extendedRight <- unfoldM (extend rightVec) spine
  mapM (dotDistort >=> dotShrink >=> dotRandomizeFill >=> dotRandomizeColor) $
    mconcat (extendedLeft ++ [spine] ++ extendedRight)
  where extend :: V2 Double
               -> Vector Dot
               -> GenArt (Maybe (Vector Dot, Vector Dot))
        extend dist prev = if null prev
          then pure Nothing
          else let less = Vec.drop 1 prev
                   extendDot (Coord center, radius, _, _) = (Coord (center + dist), radius, Fill, Nothing)
                   extended = fmap extendDot less
               in pure (Just (extended, extended))

genBox :: Coord -> GenArt Box
genBox center = do
  north <- getNorth

  let topRot   = rotation2D (pi * (1 / 6))
  let leftRot  = rotation2D (pi * (5 / 6))
  let rightRot = rotation2D (pi * (9 / 6))  -- I'm leaving this unsimplified for clarity.

  top <- Panel <$> genPanelDots center (topRot !* north) <*> pure lavender
  left <- Panel <$> genPanelDots center (leftRot !* north) <*> pure morningGlory
  right <- Panel <$> genPanelDots center (rightRot !* north) <*> pure aphotic
  pure $ Box
    { boxCenter = center
    , boxTop = top
    , boxLeft = left
    , boxRight = right
    }

--------------------
-- RENDERING
--------------------

render :: Meta (ConfigB Bare Identity) -> [Box] -> Render ()
render cfg boxes = do
  let (w, h) = bimap fromIntegral fromIntegral $ metaDimensions cfg

  setAntialias AntialiasBest
  colorRGB photic *> rectangle 0 0 w h *> fill
  mapM_ renderBox boxes

renderBox :: Box -> Render ()
renderBox (Box { .. }) = mapM_ renderPanel [boxTop, boxLeft, boxRight]

renderPanel :: Panel -> Render ()
renderPanel (Panel { .. }) = mapM_ (renderDot panelColor) panelDots

renderDot :: Color -> Dot -> Render ()
renderDot color (center, radius, fillStyle, colorOverride) = do
  newPath
  colorRGB $ case colorOverride of
    Nothing     -> color
    Just color' -> color'
  circle center radius
  case fillStyle of
    Fill   -> fill
    Hollow -> setLineWidth 2 *> stroke

--------------------
-- COLORS
--------------------

gunmetal :: Color
gunmetal = Color 0x68 0x68 0x68

lavender :: Color
lavender = Color 0xC2 0xAF 0xF0

morningGlory :: Color
morningGlory = Color 0x91 0x91 0xE9

photic :: Color
photic = Color 0x45 0x7E 0xAC

aphotic :: Color
aphotic = Color 0x2D 0x5D 0x7B
