module Linear.Matrix.Extended
  ( rotation2D
  )
where

import Linear.Matrix
import Linear.V2

-- |
-- Generate a 2D rotation matrix, in radians. Follows the convention
-- where 0π is due east and angles move counterclockwise.
--
-- See <https://en.wikipedia.org/wiki/Rotation_matrix>
rotation2D :: Floating a => a -> M22 a
rotation2D rad = V2
  -- Note that since Y increases downward in Cairo,
  -- we negate the angle passed in.
  (V2 (cos $ negate rad) (sin rad))
  (V2 (sin $ negate rad) (cos $ negate rad))
