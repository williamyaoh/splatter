module Main where

import System.Environment ( getArgs )

import Graphics.Splatter           as Splatter
import Graphics.Splatter.Warehouse as Warehouse

supportedImages :: [(String, Splatter.Generator)]
supportedImages =
  [ ("warehouse", Warehouse.generator)
  ]

main :: IO ()
main = do
  (subimageGen, subimageArgs) <- getGenArgs

  Splatter.execGenerator subimageArgs subimageGen

getGenArgs :: IO (Splatter.Generator, [String])
getGenArgs = do
  args <- getArgs
  case args of
    []            -> fail "unknown image generator"
    (img:imgArgs) -> case lookup img supportedImages of
      Just gen -> pure (gen, imgArgs)
      Nothing  -> fail "unknown image generator"
